"use strict";

const testArray = [null, "hello", 1, undefined, {}, null, 23]

function filterBy(array, dataType) {
    let processedArray = [];
    for (let i = 0; i < array.length; i++) {
        if(typeof array[i] === dataType){
            processedArray.push(array[i]);
        }
    }
    return processedArray;
}

console.log(filterBy(testArray, "undefined"));

